package com.calculate.distancecalculate.services;

import com.calculate.distancecalculate.controller.CoordinateResponse;
import com.calculate.distancecalculate.controller.CoordinatesRequest;
import com.calculate.distancecalculate.controller.DistanceRequest;
import com.calculate.distancecalculate.controller.DistanceResponse;
import com.calculate.distancecalculate.model.Postcodelatlng;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface PostService {

    public CoordinateResponse updateByPostcodeMVC(String postcode1, CoordinateResponse location);

    public DistanceRequest getDistanceMVC(String postcode1, String postcode2);

    public DistanceResponse getDistance(String postcode1, String postcode2);

    public List<Postcodelatlng> getAll();

    public Optional<Postcodelatlng> update(Postcodelatlng client, long id) throws Exception;

    public List<Postcodelatlng> queryTest(String post1, String postdoce2);

    public CoordinatesRequest updateByPostcode(String postcode1, Postcodelatlng location);

}
