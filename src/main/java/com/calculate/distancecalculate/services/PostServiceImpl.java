package com.calculate.distancecalculate.services;

import com.calculate.distancecalculate.controller.CoordinateResponse;
import com.calculate.distancecalculate.controller.DistanceRequest;
import com.calculate.distancecalculate.error.LocationNotFoundException;
import com.calculate.distancecalculate.controller.CoordinatesRequest;
import com.calculate.distancecalculate.controller.DistanceResponse;
import com.calculate.distancecalculate.model.Postcodelatlng;
import com.calculate.distancecalculate.repository.PostcodelatlngRepository;
import com.calculate.distancecalculate.util.UtilMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.calculate.distancecalculate.controller.rest.controler.Distance.km;


@Service
@Transactional
public class PostServiceImpl implements PostService {

    @Autowired
    private PostcodelatlngRepository postRepo;

    private final static double EARTH_RADIUS = 6371;


    //get 2 locations and their distance by passing postcodes
    @Override
    public DistanceRequest getDistanceMVC(String postcode1, String postcode2) {

        DecimalFormat testF = new DecimalFormat("#.00");

        Postcodelatlng location1 = postRepo.findByPostcode(postcode1);
        Postcodelatlng location2 = postRepo.findByPostcode(postcode2);

        if (location1 == null || location2 == null) {
            throw new LocationNotFoundException("Wrong or incomplete postcode: " + " postcode >>>" + postcode1 + " or postcode >>>" + postcode2 + " is invalid. Try again with proper post code");
        }

        double distance = UtilMethods.calculateDistance(location1.getLatitude(), location1.getLongitude(),
                location2.getLatitude(), location2.getLatitude());

        return new DistanceRequest(location1.getPostcode(), location2.getPostcode(), Double.parseDouble(testF.format(distance)));
    }

    //update location coordinates by postcode search
    @Override
    public CoordinateResponse updateByPostcodeMVC(String postcode1, CoordinateResponse location) {

        Postcodelatlng location1 = postRepo.findByPostcode(postcode1);
        if (location1 == null || location == null) {
            throw new LocationNotFoundException("Invalid postcode: " + postcode1 + " >>>>" + "Try again with existing UK post code");
        }

        location1.setLatitude(location.getLatitude());
        location1.setLongitude(location.getLongitude());

        return new CoordinateResponse(postcode1, location1.getLatitude(), location1.getLongitude());
    }


    //get 2 locations and their distance by passing postcodes
    @Override
    public DistanceResponse getDistance(String postcode1, String postcode2) {

        Postcodelatlng location1 = postRepo.findByPostcode(postcode1);
        Postcodelatlng location2 = postRepo.findByPostcode(postcode2);

        double distance = UtilMethods.calculateDistance(location1.getLatitude(), location1.getLongitude(),
                location2.getLatitude(), location2.getLatitude());

        return new DistanceResponse(location1.getPostcode(), location2.getPostcode(), distance, km);
    }

    //get all data by id
    @Override
    public List<Postcodelatlng> getAll() {

        //noinspection UnnecessaryLocalVariable
        List<Postcodelatlng> postList = (List<Postcodelatlng>) postRepo.findAll();
        return postList;
    }

    //update postcode with id
    @Override
    public Optional<Postcodelatlng> update(Postcodelatlng location, long id) {
        Optional<Postcodelatlng> current = postRepo.findById(id);
        if (!current.isPresent()) {
            throw new LocationNotFoundException("Invalid location: " + id);
        }
        current.get().setPostcode(location.getPostcode());

        return current;
    }

    //return object found by postcode
    @Override
    public List<Postcodelatlng> queryTest(String postcode1, String postcode2) {

        List<Postcodelatlng> postList = new ArrayList<>();
        postList.add(postRepo.findByPostcode(postcode1));
        postList.add(postRepo.findByPostcode(postcode2));

        return postList;
    }


    //update location coordinates by postcode search
    @Override
    public CoordinatesRequest updateByPostcode(String postcode1, Postcodelatlng location) {

        Postcodelatlng location1 = postRepo.findByPostcode(postcode1);
        if (location1 == null || location == null) {
            throw new LocationNotFoundException("Invalid postcode: " + postcode1);
        }

        location1.setLatitude(location.getLatitude());
        location1.setLongitude(location.getLongitude());

        @SuppressWarnings("UnnecessaryLocalVariable") CoordinatesRequest returnCoordinatesRequest = new CoordinatesRequest(location1.getLatitude(), location1.getLongitude());

        return returnCoordinatesRequest;
    }

//    //get objects by passed id's in http request
//    @Override
//    public Distance getData(Long id1, Long id2) {
//
//        Postcodelatlng firstLocation = postRepo.findById(id1).get();
//        Postcodelatlng secondLocation = postRepo.findById(id2).get();
//
//        String distance = calculateDistance(firstLocation.getLatitude(), firstLocation.getLongitude(),
//                secondLocation.getLatitude(), secondLocation.getLatitude());
//
//        return new Distance(firstLocation, secondLocation, distance);
//    }

}
