package com.calculate.distancecalculate.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;


@Configuration
public class JavaConfig {

        @Bean
        public DataSource dataSource() {
            DriverManagerDataSource driver = new DriverManagerDataSource();
            driver.setDriverClassName("org.postgresql.Driver");
            driver.setUrl("jdbc:postgresql://localhost:5432/calculate");
            driver.setUsername("postgres");
            driver.setPassword("123");
            return driver;
        }
}
