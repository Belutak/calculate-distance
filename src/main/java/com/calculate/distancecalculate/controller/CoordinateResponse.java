package com.calculate.distancecalculate.controller;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CoordinateResponse {

    @NotNull(message = "is required")
    @Size(min = 8, max = 8, message = "valid postcode must have 8 characters")
    private String postcode;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;

    public CoordinateResponse() {
    }

    public CoordinateResponse(String postcode, Double latitude, Double longitude) {
        this.postcode = postcode;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
