package com.calculate.distancecalculate.controller;

import com.calculate.distancecalculate.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@Controller
@RequestMapping("/location")
public class LocationController {

    //preprocessor that trims empty spaces of string
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @Autowired
    private PostService postServis;

    //takes input from a form and passes it to a processForm
    @RequestMapping(value = "/distance", method = RequestMethod.GET)
    public String showForm(Model theModel) {

        DistanceRequest location = new DistanceRequest();
        theModel.addAttribute("location", location);

        return "form-location";
    }

    //calls service method to calculate distance between passed values(postcodes)
    @RequestMapping(value = "/processForm", method = RequestMethod.POST)
    public String processForm(@Valid @ModelAttribute("location") DistanceRequest location,
                              BindingResult theBindingResult) {

        if (theBindingResult.hasErrors()) {

            return "form-location";
        } else {
            DistanceRequest distanceRequest = postServis.getDistanceMVC(location.getPostcode1(), location.getPostcode2());
            location.setDistance(distanceRequest.getDistance());

            return "confirmation-location";
        }
    }


    //takes input from form and passes its value to processUpdate
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String updateForm(Model theModel) {

        CoordinateResponse location = new CoordinateResponse();
        theModel.addAttribute("update", location);

        return "form-update";
    }

    //calls service method to get set values from passed coordinates
    @RequestMapping(value = "/processUpdate", method = RequestMethod.POST)
    public String processUpdate(@Valid @ModelAttribute("update") CoordinateResponse location,
                                BindingResult thebindingResult, Model model) {
        if (thebindingResult.hasErrors()) {

            return "form-update";
        }

        try{
            CoordinateResponse coordinateResponse = postServis.updateByPostcodeMVC(location.getPostcode(), location);
            location.setLatitude(coordinateResponse.getLatitude());
            location.setLongitude(coordinateResponse.getLongitude());
        }catch (Exception e){
            model.addAttribute("error", e.getMessage());
            return "update-form";
        }

            return "confirmation-update";

    }


    //test
    @RequestMapping("/home")
    public String showPage() {
        return "home";
    }
}
