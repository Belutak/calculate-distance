package com.calculate.distancecalculate.controller;

public class DistanceResponse {

    private static String km = "km";
    private String postcode1;
    private String postcode2;
    private double distance;

    public DistanceResponse() {
    }

    public DistanceResponse(String postcode1, String postcode2, double distance, String km) {
        this.postcode1 = postcode1;
        this.postcode2 = postcode2;
        this.distance = distance;
        this.km = km;
    }

    public static String getKm() {
        return km;
    }

    public static void setKm(String km) {
        DistanceResponse.km = km;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    public String getPostcode2() {
        return postcode2;
    }

    public void setPostcode2(String postcode2) {
        this.postcode2 = postcode2;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
