package com.calculate.distancecalculate.controller;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DistanceRequest {

    public static final String km = "km";
    @NotNull(message="is required")
    @Size(min = 8, max = 8, message = "valid postcode must have 8 characters")
    private String postcode1;
    @NotNull(message="is required")
    @Size(min = 8, max = 8, message = "valid postcode must have 8 characters")
    private String postcode2;

    private double distance;

    public DistanceRequest() {
    }

    public DistanceRequest(String postcode1, String postcode2, double distance) {
        this.postcode1 = postcode1;
        this.postcode2 = postcode2;
        this.distance = distance;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    public String getPostcode2() {
        return postcode2;
    }

    public void setPostcode2(String postcode2) {
        this.postcode2 = postcode2;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
