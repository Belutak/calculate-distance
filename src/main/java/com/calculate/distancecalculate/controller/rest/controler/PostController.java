package com.calculate.distancecalculate.controller.rest.controler;

import com.calculate.distancecalculate.controller.CoordinatesRequest;
import com.calculate.distancecalculate.controller.DistanceResponse;
import com.calculate.distancecalculate.services.PostService;
import com.calculate.distancecalculate.model.Postcodelatlng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


//@RestController
@RequestMapping("/calculate")
public class PostController {

    @Autowired
    PostService postService;


    //update postcode selected locations coordinates
    @PutMapping(value = "/")
    public CoordinatesRequest changeCordinates(@RequestParam(required = false, name = "postcode") String postcode1,
                                               @RequestBody @Valid Postcodelatlng location) {
        CoordinatesRequest updateLocation = postService.updateByPostcode(postcode1, location);

        return updateLocation;
    }


    //return 2 locations and distance between them by postcode
    @GetMapping(value = "/")
    public DistanceResponse testDistanceDto(@RequestParam(required = false, name = "postcode1") String postcode1,
                                            @RequestParam(required = false, name = "postcode2") String postcode2) {

        return postService.getDistance(postcode1, postcode2);
    }






//    //return 2 locations and distance between them by id
//    @GetMapping(value = "/{id1}/{id2}")
//    public ResponseEntity<Distance> getData(@PathVariable Long id1, @PathVariable Long id2) {
//
//        return ResponseEntity.ok(postService.getData(id1, id2));
//    }

    //get objects by passing postcode in http
    @GetMapping(value = "/test")
    public List<Postcodelatlng> getQueryData(@RequestParam(required = false, name = "postcode1") String postcode1,
                                             @RequestParam(required = false, name = "postcode2") String postcode2) {

        return postService.queryTest(postcode1, postcode2);
    }

    //update selected location code
    @PutMapping("/update/{id}")
    public ResponseEntity updateCode(@PathVariable long id, @RequestBody Postcodelatlng city) throws Exception {
        return ResponseEntity.ok(postService.update(city, id));
    }


    @GetMapping("/gg")
    public String hello() {
        return "Hello";
    }

//    @RequestMapping(value = "/distance", method = RequestMethod.GET)
//    public ResponseEntity<String> getQueryData(@RequestParam(required=false,name="postcode1") String postcode1,
//                                               @RequestParam(required=false,name="postcode2") String postcode2){
//
//
//        return ResponseEntity.ok(postcode1.toString() + postcode2.toString());
//    }

//
//    @RequestMapping(value = "/{postCode}", method = RequestMethod.GET)
//    public ResponseEntity<Postcodelatlng> getDataForOne(@RequestParam String postcode) {
//
//        return ResponseEntity.ok(postService.queryTestOne(postcode1);
//    }


}