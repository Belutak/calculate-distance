package com.calculate.distancecalculate.controller.rest.controler;


import com.calculate.distancecalculate.model.Postcodelatlng;

public class Distance {

    public final static String km = "km";
    private Postcodelatlng locationOne;
    private Postcodelatlng locationTwo;

    private String distanceKm;

    public Distance() {
    }


    public Distance(Postcodelatlng locationOne, Postcodelatlng locationTwo, String distanceKm) {
        this.locationOne = locationOne;
        this.locationTwo = locationTwo;
        this.distanceKm = distanceKm;
    }

    public Postcodelatlng getLocationOne() {
        return locationOne;
    }

    public void setLocationOne(Postcodelatlng locationOne) {
        this.locationOne = locationOne;
    }

    public Postcodelatlng getLocationTwo() {
        return locationTwo;
    }

    public void setLocationTwo(Postcodelatlng locationTwo) {
        this.locationTwo = locationTwo;
    }

    public String getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(String distanceKm) {
        this.distanceKm = distanceKm;
    }
}
