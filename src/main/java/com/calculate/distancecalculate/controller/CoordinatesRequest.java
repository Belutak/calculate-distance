package com.calculate.distancecalculate.controller;

public class CoordinatesRequest {

    private double latitude;
    private double longitude;

    public CoordinatesRequest() {
    }

    public CoordinatesRequest(double latidute, double longitude) {
        this.latitude = latidute;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setlatitude(double latidute) {
        this.latitude = latidute;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
