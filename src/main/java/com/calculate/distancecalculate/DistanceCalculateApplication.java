package com.calculate.distancecalculate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistanceCalculateApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistanceCalculateApplication.class, args);
	}
}
