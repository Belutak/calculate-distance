package com.calculate.distancecalculate.repository;

import com.calculate.distancecalculate.model.Postcodelatlng;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostcodelatlngRepository extends CrudRepository<Postcodelatlng, Long> {

   // @Query("SELECT t FROM Postcodelatlng t WHERE t.postcode = ?")
    public Postcodelatlng findByPostcode(String postcode1);
}
