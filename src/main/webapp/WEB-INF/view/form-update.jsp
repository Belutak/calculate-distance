<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>
    <head>
     <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <title>Update Form</title>
             <%@ include file="parts/header.jsp" %>

                <style>
                    .error {
                     color:red
                    }
                    .form-control {
                       width: 30%;
                    }
                 </style> 
    </head>

    <body>
<div class="container">
                                <p>${error!=null?error:""}</p>
    <div class="jumbotron">
        <h2>Update coordinates:</h2>

        <form:form action="processUpdate" modelAttribute="update" method="post">
             <td><form:label path="postcode">Postcode</form:label></td>
             <td> <form:input class="form-control" name="postcode1" path="postcode" placeholder="postcode" /></td>
             <td><form:errors path="postcode" cssClass="error" />

             <td><form:label path="latitude">Latitude</form:label></td>
             <td> <form:input class="form-control" name="latitude" path="latitude" placeholder="0.0"/></td>
             <td><form:errors path="latitude" cssClass="error" />

             <td><form:label path="longitude">Longitude</form:label></td>
             <td> <form:input class="form-control" name="longitude" path="longitude" placeholder="0.0"/></td>
             <td><form:errors path="longitude" cssClass="error" />


            <input type="submit" value="Submit" class="btn btn-info" />
        </form:form>
     </div>
</div>

<%@ include file="parts/footer.jsp" %>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    </body>


</html>