<!DOCTYPE html>

<html>

<head>

<link href="${pageContext.request.contextPath}design.css" rel="stylesheet" >

        <style>

            .my-footer {
                    position: fixed;
                    bottom:0;
                    width: 100%;
            }

        </style>
</head>
<body>

<!-- Footer -->
<footer class="page-footer font-small blue">

  <!-- Copyright -->
  <div class="my-footer">
  <div class="footer-copyright text-center py-3"> @2018 Copyright:
    <a href=" http://www.miticon.com//"> MITICON</a>
  </div>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->

</body>


</html>
