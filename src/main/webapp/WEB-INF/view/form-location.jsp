<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Calculate</title>
    <%@ include file="parts/header.jsp" %>

        <style>
            .error {
            color:red
             }

            .jumbotron{
            background-color: rgb(214, 245, 214);
            color: grey;
            /*background-color:none !important;*/
            }

            .form-control {
                width: 30%;
            }
         </style>


    </head>

    <body>
<div class="container">
<div class="jumbotron">
        <h2>Calculate distance between two postcodes:</h2>
        <form:form action="processForm" modelAttribute="location" method="post">

          <p> <td><form:label path="postcode1">First Postcode</form:label></td></p>
          <p>  <form:input class="form-control" name="postcode1" path="postcode1" placeholder="postcode"/> </p>
          <form:errors path="postcode1" cssClass="error"/>

           <p> <td><form:label path="postcode2">Second Postcode</form:label></td> </p>
           <p> <form:input class="form-control" name="postcode2" path="postcode2"  placeholder="postcode"/> </p>
           <form:errors path="postcode2"  cssClass="error"/>

           <input type="submit" value="Submit" class="btn btn-info"/>
        </form:form>
</div>
</div>


<div class="proba" >



</div>

    <%@ include file="parts/footer.jsp" %>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
    </html>
